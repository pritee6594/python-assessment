class Shop:
	def __init__(self, prices, validDiscounts):
		self.prices = prices
		self.validDiscounts = validDiscounts

class Basket:
	def __init__(self, items, discountCode):
		self.items = items
		self.discountCode = discountCode



def basketsSort(bList,shop):
	values = []
	for cart in bList:
		total = 0
		
		for item in cart.items:
			# mulltiply quantity with item prices and add to total 

			if item in shop.validDiscounts[cart.discountCode]:
				# check if discount is applicable on current item, subract percentage
				total += ( shop.prices[item] * (shop.validDiscounts[cart.discountCode][item]/100) ) * cart.items[item]
			else:
				# if discount not applicable, add full price
				total += shop.prices[item] * cart.items[item]

		values.append(total)

	#sort by prices
	values.sort()
	return values

shopMayorStreet = Shop(
	#product:prices dictionary
	{
	"Rice":1.00,
	"Grapes":2.10,
	"Flour":2.00,
	"Milk":2.7,
	"Oranges":0.7,
	"Eggs":0.5,
	"Candy":2.0
	},

	#allowed discounts percentage dictionary
	{
	"Eggs20" : {"Eggs":20},
	"Christmas" : {"Candy":10},
	"LuckyDay" : { "Rice" : 50, "Grapes" : 50, "Flour" : 50,	"Milk" : 50, "Oranges" : 50, "Eggs" : 50, "Candy" : 50 },
	"FreeRice" : { "Rice":100 },
	"Breakfast50" : {"Eggs":50, "Milk":50}
	}
	)


# basket objects that we will be comparing
b1 = Basket({"Flour":100,"Candy":2}, "LuckyDay")
b2 = Basket({"Rice":2,"Grapes":2,"Flour":1}, "FreeRice")
b3 = Basket({"Candy":2,"Grapes":1,"Flour":1}, "Eggs20")


# function called
print("Baskets from lowest to highest")
print(basketsSort([b1,b2,b3],shopMayorStreet))

